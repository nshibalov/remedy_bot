# coding: utf-8

import sys
import uuid
from cx_Freeze import setup, Executable


def main():
    proj_name = "remedy_bot"

    build_exe_options = {
        "excludes": ["Tkinter", "PySide", "PyQt4", "pandas", "numpy"],
        "include_files": []
    }

    if sys.platform == "win32":
        build_exe_options["include_files"].append("drivers/")

    bdist_msi_options = {
        "upgrade_code": "{{{}}}".format(
            uuid.uuid5(uuid.NAMESPACE_DNS, proj_name))
    }

    setup(
        name=proj_name,
        version="1.0",
        description=proj_name,
        author="Shibalov N. A.",
        author_email="nshibalov@gmail.com",
        options={
            "build_exe": build_exe_options,
            "bdist_msi": bdist_msi_options
        },
        executables=[
            Executable(
                "main.py",
                targetName="{}.exe".format(proj_name),
                shortcutName=proj_name,
                shortcutDir="ProgramMenuFolder"),
            ])


if __name__ == "__main__":
    main()
