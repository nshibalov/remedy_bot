# coding: utf-8

import logging

from argparse import ArgumentParser
from remedy_bot.log import LOG
from remedy_bot.config import Config
from remedy_bot.remedybot import RemedyBot
from remedy_bot.workloader import WorkLoader


def _add_driver_arg(group, default_driver_str, driver_str, help_str):
    group.add_argument(
        "--{}".format(driver_str),
        action="store_true",
        help=(
            "{} (Default)".format(help_str)
            if driver_str == default_driver_str
            else help_str))


def _str_to_driver(driver_str):
    if driver_str == Config.DRIVER_STR_FIREFOX:
        return RemedyBot.DRIVER_FIREFOX
    elif driver_str == Config.DRIVER_STR_CHROME:
        return RemedyBot.DRIVER_CHROME
    elif driver_str == Config.DRIVER_STR_IE:
        return RemedyBot.DRIVER_IE
    return RemedyBot.DRIVER_FIREFOX


def main():
    LOG.setLevel(logging.INFO)

    parser = ArgumentParser(description="RemedyBot")
    parser.add_argument(
        "works_fname",
        metavar="<Works file>",
        type=str,
        help=(
            "XLSX. Header ignored (first row). Columns (* - must): "
            "<Name*>, "
            "<Work Type*>, "
            "<Work Class*>, "
            "<Service*>, "
            "<Service impact*>, "
            "<NE>, "
            "<NE change count>, "
            "<DT begin>, "
            "<DT end>, "
            "<Performer*>"))
    parser.add_argument("-s", "--save", action="store_true", help="Save works")
    parser.add_argument(
        "-w",
        "--show-window",
        action="store_true",
        help="Show browser window (default is headless mode)")

    default_driver_str = Config().default_driver_str()

    group = parser.add_mutually_exclusive_group()
    _add_driver_arg(
        group,
        default_driver_str,
        Config.DRIVER_STR_FIREFOX,
        "Use Firefox Driver")
    _add_driver_arg(
        group,
        default_driver_str,
        Config.DRIVER_STR_CHROME,
        "Use Chrome Driver")
    _add_driver_arg(
        group,
        default_driver_str,
        Config.DRIVER_STR_IE,
        "Use IE Driver")

    args = parser.parse_args()

    works = WorkLoader().load(args.works_fname)

    driver = _str_to_driver(default_driver_str)
    if args.firefox:
        driver = RemedyBot.DRIVER_FIREFOX
    elif args.chrome:
        driver = RemedyBot.DRIVER_CHROME
    elif args.ie:
        driver = RemedyBot.DRIVER_IE

    rem_bot = RemedyBot(driver, headless=not args.show_window)
    rem_bot.run(works, args.save)


if __name__ == "__main__":
    main()
