# coding: utf-8


class WebAutomationException(Exception):
    pass


class WorkLoaderException(WebAutomationException):
    pass


class RemedyException(WebAutomationException):
    pass


class DialogException(WebAutomationException):
    pass


class ClassificatorNotFound(RemedyException):
    pass


class NENotFound(RemedyException):
    pass


class NoSuchItem(RemedyException):
    pass
