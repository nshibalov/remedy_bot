# coding=utf-8

import logging


def _init_logger(name):
    logger = logging.getLogger(name)

    handler = logging.StreamHandler()
    handler.setFormatter(logging.Formatter(
        "%(asctime)s - %(levelname)s - "
        "[%(module)s:%(lineno)d] %(message)s"))

    logger.addHandler(handler)
    return logger


LOG = _init_logger("remedy_bot")
