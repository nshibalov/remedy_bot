# coding: utf-8

from selenium.webdriver.common.keys import Keys

from remedy_bot.log import LOG
from remedy_bot.exceptions import ClassificatorNotFound, NENotFound


class WorkPage(object):
    def __init__(self, remedy_bot, save_mode):
        self.remedy_bot = remedy_bot
        self.save_mode = save_mode

    def new(self):
        self.remedy_bot.click(".newrequest")
        self.remedy_bot.click_popup_button(
            "Подтвердить сохранение запроса", "Нет")

    def save(self):
        if not self.save_mode:
            return

        self.remedy_bot.click_link("Сохранить")
        self.remedy_bot.wait_for_text(".TBTopBarStatusMode", "Изменить")

    def select_work_type(self, work_type):
        self.remedy_bot.dropdown_select("#arid_WIN_0_536870930", work_type)

    def select_work_classificator(self, code):
        current_window_handle = self.remedy_bot.current_window_handle()

        try:
            self.remedy_bot.click("#reg_img_536870872")

            window_handles = self.remedy_bot.window_handles()
            self.remedy_bot.switch_to_window(window_handles[-1])

            self.remedy_bot.click_link("Выбор по вводу")

            table = self.remedy_bot.wait_for("#WIN_0_536870922")
            elements = [
                elem
                for elem in table.find_elements_by_xpath(
                    "//span[text() = '{}']".format(code))
                if elem.text == code
            ]

            if not elements:
                self.remedy_bot.click("#reg_img_536870906")  # Close icon
                raise ClassificatorNotFound("No such code: '{}'".format(code))

            self.remedy_bot.click_element(elements[0], scroll_to=True)

            self.remedy_bot.click("#reg_img_536870907")  # Save icon

        finally:
            self.remedy_bot.switch_to_window(current_window_handle)

    def select_service(self, service):
        self.remedy_bot.dropdown_select("#arid_WIN_0_536870956", service)

    def select_service_impact(self, service_impact):
        self.remedy_bot.dropdown_select(
            "#arid_WIN_0_540000012", service_impact)

    def set_description(self, description):
        self.remedy_bot.send_input("#arid_WIN_0_8", description)

    def set_time_begin(self, time_begin):
        self.remedy_bot.send_input("#arid_WIN_0_540000045", time_begin)
        self.remedy_bot.send_input("#arid_WIN_0_536870939", time_begin)

    def set_time_end(self, time_end):
        self.remedy_bot.send_input("#arid_WIN_0_537000005", time_end)
        self.remedy_bot.send_input("#arid_WIN_0_536870942", time_end)

    def _select_ne(self, name):
        current_window_handle = self.remedy_bot.current_window_handle()

        try:
            self.remedy_bot.click("#reg_img_536870900")

            window_handles = self.remedy_bot.window_handles()
            self.remedy_bot.switch_to_window(window_handles[-1])

            self.remedy_bot.send_input("#arid_WIN_0_536870935", name)
            self.remedy_bot.send_input("#arid_WIN_0_536870935", Keys.RETURN)

            self.remedy_bot.wait_for_text(
                "#WIN_0_536870924 .TableHdrL", "Showing")

            table = self.remedy_bot.find_element("#WIN_0_536870924")
            elements = [
                elem
                for elem in table.find_elements_by_xpath(
                    "//span[text() = '{}']".format(name))
                if elem.text == name
            ]

            if not elements:
                self.remedy_bot.click_link("Закрыть")
                raise NENotFound("No such NE: '{}'".format(name))

            self.remedy_bot.click_element(elements[0], scroll_to=True)
            self.remedy_bot.click_link("Сохранить")

        except NENotFound as error:
            LOG.error(error)
            return False

        finally:
            self.remedy_bot.switch_to_window(current_window_handle)

        return True

    def _set_ne_name(self, name):
        self.remedy_bot.wait_for_attribute_value(
            "#arid_WIN_0_1000000065", "readonly", None)
        self.remedy_bot.send_input("#arid_WIN_0_1000000065", name, clear=True)

    def set_ne(self, name):
        if not self._select_ne(name):
            if self._select_ne("ANY_BTS_Moskva_Moskva"):
                self._set_ne_name(name)
            return False
        return True

    def set_operational_context(self, operational_context):
        self.remedy_bot.send_input(
            "#arid_WIN_0_540000049", operational_context)

    def set_performer_group(self, group):
        self.remedy_bot.send_input("#arid_WIN_0_536870919", group)
        self.remedy_bot.send_input("#arid_WIN_0_536870919", Keys.RETURN)

    def set_performer(self, performer):
        self.remedy_bot.send_input("#arid_WIN_0_536870933", performer)
        self.remedy_bot.send_input("#arid_WIN_0_536870933", Keys.RETURN)

    def set_ne_changes_count(self, count):
        self.remedy_bot.send_input("#arid_WIN_0_810100", count)

    def set_subsystem(self, subsystem):
        self.remedy_bot.send_input("#arid_WIN_0_777000047", subsystem)

    def set_region(self, region):
        self.remedy_bot.send_input("#arid_WIN_0_777000045", region)

    def fill_assigment_tab(self, work):
        self.remedy_bot.click_link("Назначение работы")

        self.set_performer_group(work.performer_group)
        self.set_performer(work.performer)

        self.set_operational_context(work.context)

    def fill_ne_tab(self, work):
        self.remedy_bot.click_link("Объект сети. Описание")

        self.set_ne_changes_count(work.ne_changes_count)

    def make(self, work):
        self.new()

        self.select_work_type(work.work_type)
        self.select_work_classificator(work.work_classificator)

        self.select_service(work.service)
        self.select_service_impact(work.service_impact)

        self.set_description(work.name)
        self.set_time_begin(work.dt_begin)
        self.set_time_end(work.dt_end)

        self.set_ne(work.ne_name)

        self.fill_assigment_tab(work)
        self.fill_ne_tab(work)

        self.save()
