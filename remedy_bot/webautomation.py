# coding: utf-8

import time

from io import BytesIO
from PIL import Image

from selenium.common.exceptions import TimeoutException
from selenium.webdriver import Firefox, Chrome, Ie, ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.ie.options import Options as IeOptions

from remedy_bot.log import LOG
from remedy_bot.config import Config
from remedy_bot.exceptions import WebAutomationException


def _make_driver(driver_type, opts_type, path, headless):
    opts = opts_type()
    if headless:
        opts.set_headless()
    return driver_type(options=opts, executable_path=path)


def _make_firefox_driver(config, headless):
    return _make_driver(
        Firefox, FirefoxOptions, config.geckodriver_path(), headless)


def _make_chrome_driver(config, headless):
    return _make_driver(
        Chrome, ChromeOptions, config.chromedriver_path(), headless)


def _make_ie_driver(config, headless):
    return _make_driver(Ie, IeOptions, config.iedriver_path(), headless)


# pylint: disable=too-many-return-statements
def _locator_to_str(locator):
    by_type, selector = locator
    if by_type == By.ID:
        return "#{}".format(selector)
    elif by_type == By.CLASS_NAME:
        return ".{}".format(selector)
    elif by_type == By.CSS_SELECTOR:
        return selector
    elif by_type == By.LINK_TEXT:
        return "link '{}'".format(selector)
    elif by_type == By.PARTIAL_LINK_TEXT:
        return "link '*{}*'".format(selector)
    elif by_type == By.NAME:
        return "[name == {}]".format(selector)
    elif by_type == By.TAG_NAME:
        return "[tag == {}]".format(selector)
    return "locator({}, '{}')".format(by_type, selector)
# pylint: enable=too-many-return-statements


# pylint: disable=too-few-public-methods
class _WaitForAttributeValue(object):
    def __init__(self, locator, attribute, value):
        self._locator = locator
        self._attribute = attribute
        self._value = value

    def __call__(self, driver):
        element = driver.find_element(*self._locator)
        if element.get_attribute(self._attribute) == self._value:
            return True
        return False
# pylint: enable=too-few-public-methods


# pylint: disable=too-many-public-methods
class WebAutomation(object):
    DRIVER_FIREFOX = 0
    DRIVER_CHROME = 1
    DRIVER_IE = 2

    TIMEOUT = 10

    def __init__(self, driver_type=DRIVER_FIREFOX, headless=None):
        self.driver_type = driver_type
        self.driver = None

        config = Config()

        if headless is None:
            headless = config.headless()

        if self.driver_type == self.DRIVER_FIREFOX:
            self.driver = _make_firefox_driver(config, headless)
        elif self.driver_type == self.DRIVER_CHROME:
            self.driver = _make_chrome_driver(config, headless)
        elif self.driver_type == self.DRIVER_IE:
            self.driver = _make_ie_driver(config, headless)
        else:
            raise WebAutomationException(
                "Unknown driver type: '{}'".format(driver_type))

        self._setup_driver(headless)

    def _setup_driver(self, headless):
        if self.driver is None:
            return

        if headless:
            self.driver.set_window_size(1920, 1080)

    def is_firefox(self):
        return self.driver_type == self.DRIVER_FIREFOX

    def is_chrome(self):
        return self.driver_type == self.DRIVER_CHROME

    def set_url(self, url):
        self.driver.get(url)

    def window_handles(self):
        return self.driver.window_handles

    def current_window_handle(self):
        return self.driver.current_window_handle

    def switch_to_window(self, handle):
        self.driver.switch_to.window(handle)

    def title(self):
        return self.driver.title

    def sleep_for(self, seconds):
        if seconds:
            time.sleep(seconds)

    def find_element(self, selector):
        return self.driver.find_element_by_css_selector(selector)

    def find_elements(self, selector):
        return self.driver.find_elements_by_css_selector(selector)

    def wait_for_alert(self, timeout):
        LOG.debug("Waiting for Alert...")
        try:
            WebDriverWait(self.driver, timeout).until(EC.alert_is_present())
            return self.driver.switch_to.alert
        except TimeoutException:
            pass
        return None

    def wait_for_staleness(self, element, timeout=TIMEOUT):
        return WebDriverWait(self.driver, timeout).until(
            EC.staleness_of(element),
            "Timeout ({}) waiting for staleness of {}".format(
                timeout, element))

    def wait_for_element(self, locator, timeout=TIMEOUT, clickable=True):
        name = _locator_to_str(locator)
        LOG.debug("Waiting for %s...", name)
        func = EC.presence_of_element_located
        if clickable:
            func = EC.element_to_be_clickable
        return WebDriverWait(self.driver, timeout).until(
            func(locator),
            "Timeout ({}) waiting for {}".format(timeout, name))

    def wait_for_elements(self, locator, timeout=TIMEOUT):
        name = _locator_to_str(locator)
        LOG.debug("Waiting for all %s...", name)
        return WebDriverWait(self.driver, timeout).until(
            EC.presence_of_all_elements_located(locator),
            "Timeout ({}) waiting for all {}".format(timeout, name))

    def wait_for(self, selector, **kwargs):
        return self.wait_for_element(
            (By.CSS_SELECTOR, selector), **kwargs)

    def wait_for_id(self, element_id, **kwargs):
        return self.wait_for_element(
            (By.ID, element_id), **kwargs)

    def wait_for_link_text(self, text, **kwargs):
        return self.wait_for_element(
            (By.LINK_TEXT, text), **kwargs)

    def wait_for_all(self, selector, **kwargs):
        return self.wait_for_elements(
            (By.CSS_SELECTOR, selector), **kwargs)

    def wait_for_all_id(self, element_id, **kwargs):
        return self.wait_for_elements(
            (By.ID, element_id), **kwargs)

    def wait_for_all_link_text(self, text, **kwargs):
        return self.wait_for_elements(
            (By.LINK_TEXT, text), **kwargs)

    def wait_for_attribute_value(
            self, selector, attribute, value, timeout=TIMEOUT):

        locator = (By.CSS_SELECTOR, selector)
        name = "attribute '{}' has value '{}' in {}".format(
            attribute, value, _locator_to_str(locator))

        return WebDriverWait(self.driver, timeout).until(
            _WaitForAttributeValue(locator, attribute, value),
            "Timeout ({}) waiting for {}".format(timeout, name))

    def wait_for_text(self, selector, text, timeout=TIMEOUT):
        locator = (By.CSS_SELECTOR, selector)
        name = "text '{}' in {}".format(text, _locator_to_str(locator))
        LOG.debug("Waiting for %s...", name)
        return WebDriverWait(self.driver, timeout).until(
            EC.text_to_be_present_in_element(locator, text),
            "Timeout ({}) waiting for {}".format(timeout, name))

    def screenshot(self):
        png_bytes = self.driver.get_screenshot_as_png()
        return Image.open(BytesIO(png_bytes))

    def send_input(self, selector, value, clear=False):
        element = self.wait_for(selector)
        if clear:
            element.clear()
        element.send_keys(value)

    def click_element(self, element, scroll_to=False):
        if self.is_firefox() and scroll_to:
            self.driver.execute_script(
                "arguments[0].scrollIntoView(true);",
                element)

        ActionChains(self.driver) \
            .move_to_element(element) \
            .click() \
            .perform()

    def click(self, selector, **kwargs):
        self.click_element(self.wait_for(selector), **kwargs)

    def click_link(self, text, **kwargs):
        self.click_element(self.wait_for_link_text(text), **kwargs)

    def action_chains(self):
        return ActionChains(self.driver)

    def run(self, works, save):
        pass

    def close(self):
        self.driver.close()
# pylint: enable=too-many-public-methods
