# coding: utf-8

import re

import datetime
import openpyxl

from remedy_bot.log import LOG
from remedy_bot.work import Work
from remedy_bot.exceptions import WorkLoaderException


def _load_wbook(fname):
    return openpyxl.load_workbook(
        filename=fname,
        read_only=True,
        data_only=True)


def _to_int(column, default=0):
    try:
        return int(column.value)
    except (ValueError, TypeError):
        pass
    return default


def _to_str(column, strip=True):
    if column.value is not None:
        return str(column.value).strip() if strip else str(column.value)
    return None


def _to_datettime(column):
    if isinstance(column.value, datetime.datetime):
        return column.value
    elif column.value is None:
        return None

    value = str(column.value)

    try:
        return datetime.datetime.strptime(value, Work.DT_FULL)
    except ValueError:
        try:
            return datetime.datetime.strptime(value, Work.DT_TIME)
        except ValueError:
            pass

    return None


def _error(row_id, text):
    raise WorkLoaderException("  Строка {}: {}".format(row_id, text))


class WorkLoader(object):
    def _parse_context(self, row):
        if len(row) >= 11:
            context = _to_str(row[10], "")
            if context:
                return context
        return None

    def parse_row(self, row_id, row):
        name = _to_str(row[0])
        if not name:
            _error(row_id, "Ну указано имя работы")

        work_type = _to_str(row[1])
        if not work_type:
            _error(row_id, "Ну указан тип работы")

        work_classificator = _to_str(row[2])
        if not work_classificator:
            _error(row_id, "Ну указан классификатор работы")
        elif re.match(r"^(?:\d+[.-]?)+$", work_classificator):
            work_classificator = re.sub(r"[.-]", ".", work_classificator)

        service = _to_str(row[3])
        if not service:
            _error(row_id, "Ну указан тип сервиса")

        service_impact = _to_str(row[4])
        if not service_impact:
            _error(row_id, "Ну указано влияние на сервис")

        performer = _to_str(row[9])
        if not performer:
            _error(row_id, "Ну указан исполнитель")

        performer_group = _to_str(row[10])
        if not performer_group:
            performer_group = Work.PERFORMER_GROUP_DEFAULT

        work = Work(name, work_type, work_classificator)
        work.service = service
        work.service_impact = service_impact
        work.performer = performer
        work.performer_group = performer_group
        work.ne_name = _to_str(row[5])
        work.ne_changes_count = _to_int(row[6])
        work.dt_begin = _to_datettime(row[7])
        work.dt_end = _to_datettime(row[8])
        work.context = self._parse_context(row)

        return work

    # pylint: disable=too-many-locals
    def load(self, fname):
        LOG.info("Loading works from '%s'...", fname)

        wbook = _load_wbook(fname)
        wsheet = wbook.active

        ret = []
        for row_id, row in enumerate(wsheet.iter_rows(row_offset=1), 1):
            try:
                ret.append(self.parse_row(row_id, row))
            except WorkLoaderException as error:
                LOG.info(error)

        LOG.info("Done.")
        return ret
    # pylint: enable=too-many-locals
