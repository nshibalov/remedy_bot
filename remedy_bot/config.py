# coding=utf-8

import os
from configparser import ConfigParser

from remedy_bot.log import LOG
from remedy_bot.utils import get_exe_dir


class Config(ConfigParser):
    FNAME = "config.ini"
    DIR_DRIVERS = "drivers"

    SECTION_GENERAL = "General"
    SECTION_REMEDY = "Remedy"

    OPTION_HEADLESS = "headless"
    OPTION_FIREFOX_PATH = "firefox_path"
    OPTION_GECKODRIVER_PATH = "geckodriver_path"
    OPTION_CHROMEDRIVER_PATH = "chromedriver_path"
    OPTION_IEDRIVER_PATH = "iedriver_path"
    OPTION_JSESSIONID = "JSESSIONID"

    OPTION_DEFAULT_DRIVER = "default_driver"
    DRIVER_STR_FIREFOX = "firefox"
    DRIVER_STR_CHROME = "chrome"
    DRIVER_STR_IE = "ie"
    DRIVER_STR_DEFAULT = DRIVER_STR_FIREFOX

    def __init__(self):
        ConfigParser.__init__(self, allow_no_value=True)

        config_dir = os.path.join(os.path.expanduser("~"), ".remedy_bot")
        if not os.path.exists(config_dir):
            os.mkdir(config_dir)

        self._fname = os.path.expanduser(
            os.path.join(config_dir, "config.ini"))

        self.read(self._fname)

    def _get_option(self, section, option, default=None, data_type=None):
        if not self.has_section(section):
            self.add_section(section)
            self.save()

        if not self.has_option(section, option):
            self.set(section, option, default)
            self.save()

        if data_type == bool:
            return self.getboolean(section, option)
        return self.get(section, option)

    def _set_option(self, section, option, value):
        if self._get_option(section, option) != value:
            self.set(section, option, value)
            self.save()
            return True
        return False

    @property
    def fname(self):
        return self._fname

    def save(self):
        with open(self.fname, "w") as ofile:
            self.write(ofile)

    def headless(self):
        return self._get_option(
            self.SECTION_GENERAL,
            self.OPTION_HEADLESS,
            "true",
            bool)

    def default_driver_str(self):
        driver_str = self._get_option(
            self.SECTION_GENERAL,
            self.OPTION_DEFAULT_DRIVER,
            self.DRIVER_STR_DEFAULT)

        driver_strings = [
            self.DRIVER_STR_FIREFOX,
            self.DRIVER_STR_CHROME,
            self.DRIVER_STR_IE
        ]

        if driver_str not in driver_strings:
            LOG.error(
                "Unknown driver str: '%s'. "
                "Available drivers: %s. "
                "Selecting: %s",
                driver_str,
                ", ".join(driver_strings),
                self.DRIVER_STR_DEFAULT)
            return self.DRIVER_STR_DEFAULT

        return driver_str

    def geckodriver_path(self):
        return self._get_option(
            self.SECTION_GENERAL,
            self.OPTION_GECKODRIVER_PATH,
            os.path.join(get_exe_dir(), self.DIR_DRIVERS, "geckodriver.exe"))

    def chromedriver_path(self):
        return self._get_option(
            self.SECTION_GENERAL,
            self.OPTION_CHROMEDRIVER_PATH,
            os.path.join(get_exe_dir(), self.DIR_DRIVERS, "chromedriver.exe"))

    def iedriver_path(self):
        return self._get_option(
            self.SECTION_GENERAL,
            self.OPTION_IEDRIVER_PATH,
            os.path.join(
                get_exe_dir(), self.DIR_DRIVERS, "IEDriverServer.exe"))

    def jsessionid(self):
        return self._get_option(self.SECTION_REMEDY, self.OPTION_JSESSIONID)

    def set_jsessionid(self, jsessionid):
        return self._set_option(
            self.SECTION_REMEDY, self.OPTION_JSESSIONID, jsessionid)
