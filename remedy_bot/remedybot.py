# coding: utf-8

from getpass import getpass

from selenium.common.exceptions import (
    UnexpectedAlertPresentException,
    NoSuchElementException,
    TimeoutException
)

from remedy_bot.log import LOG
from remedy_bot.utils import get_user_name
from remedy_bot.config import Config
from remedy_bot.workpage import WorkPage
from remedy_bot.exceptions import (
    WebAutomationException,
    DialogException,
    NoSuchItem
)
from remedy_bot.webautomation import WebAutomation


class RemedyBot(WebAutomation):
    TIMEOUT_ALERT = 1
    TIMEOUT_POPUP = 1

    def __init__(self, *args, **kwargs):
        WebAutomation.__init__(self, *args, **kwargs)

    def load_cookies(self):
        jsessionid = Config().jsessionid()
        if jsessionid is None:
            return

        self.driver.add_cookie({
            "name": "JSESSIONID",
            "path": "/arsys",
            "value": jsessionid
        })

    def save_cookies(self):
        config = Config()

        for cookie in self.driver.get_cookies():
            if cookie["name"] != "JSESSIONID":
                continue

            jsessionid = cookie["value"]
            if jsessionid != config.jsessionid():
                config.set_jsessionid(jsessionid)
                break
        else:
            return

        config.save()

    def list_popups(self, timeout=TIMEOUT_POPUP):
        ret = []
        try:
            frame = self.wait_for(".DIVPopup iframe", timeout=timeout)
            title = self.find_element(".DIVPopup .title").text.strip()
            try:
                self.driver.switch_to.frame(frame)
                message = self.find_element("#PopupMsgBox").text.strip()
                ret.append((title, message))
            finally:
                self.driver.switch_to.default_content()
        except TimeoutException:
            pass
        return ret

    def click_popup_button(
            self, popup_title, button_text, timeout=TIMEOUT_POPUP):
        try:
            frame = self.wait_for(".DIVPopup iframe", timeout=timeout)
            title = self.find_element(".DIVPopup .title").text.strip()
            if title != popup_title:
                return

            try:
                self.driver.switch_to.frame(frame)
                self.click_link(button_text)
            finally:
                self.driver.switch_to.default_content()
        except TimeoutException:
            pass

    def dropdown_items(self):
        return {
            x.text: x
            for x in self.wait_for_all("td.MenuEntryName")
            if x.text
        }

    def dropdown_select(self, selector, item):
        self.click(selector)

        try:
            self.wait_for_text(".MenuTableBody", item)
            element = self.find_element(".MenuTableBody")
            element = element.find_element_by_xpath(
                "//td[contains(text(), '{}')]".format(item))
            self.click_element(element)
            self.wait_for_staleness(element)
            return True

        except TimeoutException:
            raise NoSuchItem("No such item: '{}'".format(item))

        return False

    def click_menu_link(self, text):
        try:
            frame = self.wait_for("div iframe[title='Application List']")
            self.driver.switch_to.frame(frame)
            self.click_link(text)
        finally:
            self.driver.switch_to.default_content()

    def _check_alert(self):
        alert = self.wait_for_alert(self.TIMEOUT_ALERT)
        if alert is None:
            return False

        LOG.warning(alert.text)
        if "ARERR 9093" in alert.text:
            alert.accept()  # rewrite user

        return True

    def _check_dlg(self):
        try:
            for title, message in self.list_popups():
                if title == "BMC Remedy User - Ошибка":
                    raise DialogException("{}: {}".format(title, message))
        except TimeoutException:
            pass

    def login(self, _login, _password):
        self.load_cookies()

        self.send_input("#username-id", _login)
        self.send_input("#pwd-id", _password)
        self.click("#login")

        if not self._check_alert():
            self._check_dlg()

        self.save_cookies()

    def create_works(self, works, save):
        work_page = WorkPage(self, save)

        for work_id, work in enumerate(works, 1):
            LOG.info("Work %d/%d: %s", work_id, len(works), work.name)
            work_page.make(work)

    def exit(self):
        try:
            self.click("#TBlogout")

            try:
                self.click_popup_button(
                    "Подтвердить сохранение запроса", "Да")
            except TimeoutException:
                pass

            self.click_link("Вернуться на домашнюю страницу")
        except TimeoutException:
            pass

    def _get_login(self):
        default_login = get_user_name()
        login = input("Login (default is '{}'): ".format(default_login))
        if login:
            return login
        return default_login

    def run(self, works, save):
        login = self._get_login()
        password = getpass()

        need_exit = True
        show_screenshot = True

        try:
            self.set_url("http://remedy")
            self.login(login, password)
            self.click_menu_link("Создать Работу")

            self.create_works(works, save=save)

            show_screenshot = False

        except (
                UnexpectedAlertPresentException,
                NoSuchElementException,
                TimeoutException) as error:
            LOG.error(error)

        except DialogException as error:
            LOG.error(error)
            need_exit = False

        except WebAutomationException as error:
            LOG.error(error)

        finally:
            if need_exit:
                self.exit()

            if show_screenshot:
                self.screenshot().show()

            self.close()
