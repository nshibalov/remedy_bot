# coding=utf-8

import os
import sys
import getpass


def get_user_name():
    return getpass.getuser()


def get_exe_fname():
    return os.path.abspath(sys.argv[0])


def get_exe_dir():
    return os.path.dirname(get_exe_fname())
