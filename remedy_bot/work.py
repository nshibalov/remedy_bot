# coding: utf-8

import re
import datetime


class Work(object):
    DT_FULL = "%d.%m.%Y %H:%M"
    DT_TIME = "%H:%M"

    CONTEXT_NSN = "NSN"
    CONTEXT_NSN_2G = "NSN 2G"
    CONTEXT_NSN_3G = "NSN 3G"
    CONTEXT_NSN_4G = "NSN LTE"
    CONTEXT_DEFAULT = CONTEXT_NSN_2G

    PERFORMER_GROUP_DEFAULT = r"ЮГ\ЕЦУС\ОМОД\ЦУБД"

    def __init__(
            self,
            name,
            work_type,
            work_classificator):

        self.name = name
        self.work_type = work_type
        self.work_classificator = work_classificator
        self.service = None
        self.service_impact = None
        self.performer = None
        self.performer_group = None
        self.ne_name = None
        self.ne_change_count = 0
        self._dt_begin = None
        self._dt_end = None
        self._context = None

    @property
    def dt_begin(self):
        if self._dt_begin is None:
            now = datetime.datetime.now()

            hour = now.hour
            minute = now.minute
            guard = 2

            if minute < 30:
                if 30 - minute < guard:
                    hour += 1
                    minute = 0
                else:
                    minute = 30
            else:
                hour += 1
                minute = 30 if 60 - minute < guard else 0

            return datetime.datetime(
                now.year,
                now.month,
                now.day,
                hour if hour < 24 else 0,
                minute).strftime(self.DT_FULL)
        return self._dt_begin.strftime(self.DT_FULL)

    @dt_begin.setter
    def dt_begin(self, dt_begin):
        if not isinstance(dt_begin, datetime.datetime):
            dt_begin = None
        self._dt_begin = dt_begin

    @property
    def dt_end(self):
        if self._dt_end is None:
            now = datetime.datetime.now()
            return datetime.datetime(
                now.year,
                now.month,
                now.day,
                19).strftime(self.DT_FULL)
        return self._dt_end.strftime(self.DT_FULL)

    @dt_end.setter
    def dt_end(self, dt_end):
        if not isinstance(dt_end, datetime.datetime):
            dt_end = None
        self._dt_end = dt_end

    # pylint: disable=too-many-return-statements
    @property
    def context(self):
        if self._context is not None:
            return self._context

        if self.ne_name:
            lower = self.ne_name.lower()
            if lower.startswith("bsc"):
                return self.CONTEXT_NSN_2G
            elif lower.startswith("rnc"):
                return self.CONTEXT_NSN_3G
            elif lower in ("enode_b", "enodeb"):
                return self.CONTEXT_NSN_4G

            if re.match(r"BTS_\d+_\d+_(?:[GD])+", self.ne_name, re.I):
                return self.CONTEXT_NSN_2G
            if re.match(r"BTS_\d+_\d+_(?:U\d*)+", self.ne_name, re.I):
                return self.CONTEXT_NSN_3G
            elif re.match(r"BTS_\d+_\d+_(?:L\d+T?)+", self.ne_name, re.I):
                return self.CONTEXT_NSN_4G
            elif re.match(r"BSC_\d+", self.ne_name, re.I):
                return self.CONTEXT_NSN_4G

        return self.CONTEXT_DEFAULT
    # pylint: enable=too-many-return-statements

    @context.setter
    def context(self, context):
        self._context = context
